package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController {
	@FXML
	private Button login_button;
	
	@FXML
	private PasswordField password_input;
	
	@FXML
	private TextField username_input;
	
	@FXML
	private Label errorField;
	
	@FXML
	public void login(ActionEvent action) {
		this.errorField.setText("Login");
	}
}
