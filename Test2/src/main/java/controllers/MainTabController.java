package controllers;

import clases.NumberOfCombination;
import helpers.Functionality;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public class MainTabController {
	@FXML
	private Accordion accordionMenu;
	
	@FXML
	private Tab mainTab;
	
	@FXML
	private void initialize() {
		for (NumberOfCombination t : Functionality.allNumbers) {
			
			String title = "Number: ";
			title += t.getNumber() <= 9? "0" + t.getNumber() : t.getNumber();
			
			TitledPane rootPane = new TitledPane(title , null);
			rootPane.setCollapsible(true);
			rootPane.setExpanded(true);
			rootPane.setAnimated(true);
			
			AnchorPane secondaryPane = new AnchorPane();
			
			Label labelString = new Label();
			labelString.setText("This number is in " + t.getCounted() + " tickets, ");
			labelString.setText(labelString.getText() + "this is a " + t.getPercentage());
			
			labelString.setLayoutX(10);
			labelString.setLayoutY(10);
			
			TableView table = new TableView();
			table.setLayoutX(10);
			table.setLayoutY(40);
			
			TableColumn numberColumn = new TableColumn("Number");
			TableColumn countedColumn = new TableColumn("Counted");
			TableColumn percentageColumn = new TableColumn("Percentage");
			
			numberColumn.setCellValueFactory(new PropertyValueFactory<NumberOfCombination, Integer>("number"));
			countedColumn.setCellValueFactory(new PropertyValueFactory<NumberOfCombination, Long>("counted"));
			percentageColumn.setCellValueFactory(new PropertyValueFactory<NumberOfCombination, String>("percentage"));
			
			numberColumn.getStyleClass().add("columnTable");
			countedColumn.getStyleClass().add("columnTable");
			percentageColumn.getStyleClass().add("columnTable");
			
			table.getColumns().add(numberColumn);
			table.getColumns().add(countedColumn);
			table.getColumns().add(percentageColumn);
			
			table.setItems(t.getCalculatedOtherNumbers());
			
			table.setPrefHeight(120);
			
			secondaryPane.getChildren().add(table);
			secondaryPane.getChildren().add(labelString);
			rootPane.setContent(secondaryPane);
			
			accordionMenu.getPanes().add(rootPane);
			
		}
		
		accordionMenu.setExpandedPane(accordionMenu.getPanes().get(0));
	}
}
