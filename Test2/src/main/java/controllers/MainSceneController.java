package controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;

import clases.MyDate;
import clases.Ticket;
import helpers.Functionality;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;

public class MainSceneController {
	@FXML
	private TableView ticketsTable;
	
	@FXML
	private TableColumn ticketDateColumn;
	
	@FXML
	private TableColumn ticketCombinationColumn;
	
	@FXML
	private TableColumn ticketRepaymentColumn;
	
	@FXML
	private Label messageViewer;
	
	@FXML
	private TabPane allTabs;
	
	@FXML
	private TitledPane chargedTicketsPanel;
	
	private ObservableList<Ticket> allTickets;
	
	@FXML
	private void initialize() {
		
		// prepareTicketsTable
		this.ticketDateColumn.setCellValueFactory(new PropertyValueFactory<Ticket, MyDate>("date"));
		this.ticketCombinationColumn.setCellValueFactory(new PropertyValueFactory<Ticket, String>("numbers"));
		this.ticketRepaymentColumn.setCellValueFactory(new PropertyValueFactory<Ticket, Integer>("repayment"));

		allTickets = Functionality.chargeAllTickets();
		ticketsTable.setItems(allTickets);
		
		chargedTicketsPanel.setText("Charged Tickets " + allTickets.size());
		
		//modifyTabPane
		try {
			allTabs.getTabs().add((Tab) FXMLLoader.load(Paths.get("src/main/resources/scenes/MainTab.fxml").toUri().toURL()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void showConfiguration() {
		Functionality.showConfigurationFile();
	}
	
	@FXML
	private void updateSourceFile() {
		Functionality.updateSourceFile();
		allTabs.getTabs().clear();
		try {
			allTabs.getTabs().add((Tab) FXMLLoader.load(Paths.get("src/main/resources/scenes/MainTab.fxml").toUri().toURL()));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void tmp() {
		Functionality.workWithTheTickets();
	}
}
