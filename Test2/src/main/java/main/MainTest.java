package main;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import clases.Ticket;
import helpers.Readers;

public class MainTest {

	private static final String configurationPath = "src/main/resources/configuration.txt";
	private static String sourceWebFile;
	private static Path localWebFile;
	
	private static List<Ticket> allTickets = new ArrayList<Ticket>();
	
	public static void main(String[] args) {
		chargeConfiguration();
		chargeAllTickets();
		printAllTickets();
	}
	
	private static void chargeConfiguration() {
		try {
			File configuration = new File(configurationPath);
			Queue<String> lines = Readers.readFile(configuration);
			
			do {
				String line = lines.poll();
				String[] params = line.split(" ");
				
				switch(params[0]) {
					case "sourceDownloadResults:":
						sourceWebFile = params[1];
					break;
					case "locationOfFileInThisMachine:":
						localWebFile = Paths.get(params[1]);
					break;
				}
				
				
			} while (lines.size() > 0);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void chargeAllTickets() {
		try {
			//Readers.downloadWebFileToLocaFile(new URL(sourceWebFile), localWebFile);
			
			Queue<String> lines = Readers.readFile(localWebFile.toFile());
			
			do {
				String line = lines.poll();
				
				if (line.charAt(0) >= 48 && line.charAt(0) <= 57) {
					Ticket t = new Ticket(line, ",");
					allTickets.add(t);
				}
				
			} while(lines.size() > 0);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	
	private static void printAllTickets() {
		for(Ticket t: allTickets) {
			System.out.println(t);
		}
	}
}
