package main;

import java.nio.file.Paths;

import helpers.Functionality;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	
	public static void main(String[] args) {
		if (Functionality.chargeConfiguration()) Application.launch(args);
		else System.out.println("Error in the configuration file.");
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setResizable(true);
		primaryStage.setTitle(Functionality.appName);
		
		Parent root = FXMLLoader.load(Paths.get("src/main/resources/scenes/MainScene.fxml").toUri().toURL());
		Scene rootScene = new Scene(root);
		
		primaryStage.setScene(rootScene);
		primaryStage.show();
	}
}
