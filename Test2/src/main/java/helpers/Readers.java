package helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.Queue;

public class Readers {
	
	public static Queue<String> readFile(File f) {
		Queue<String> lines = new LinkedList<String>();
		
		try {
			if (!f.exists()) Functionality.updateSourceFile();
			if (f.isDirectory()) throw new Exception("The file is a Directory.");
			
			FileReader fr = null;
			BufferedReader br = null; 
			
			try {
				fr = new FileReader(f);
				br = new BufferedReader(fr);
				
				while (br.ready()) {
					lines.add(br.readLine());
				}
				
			}
			catch(Exception e) { // exception catched
				e.printStackTrace();
			}
			finally {
				if (fr != null) {
					fr.close();
				}
				
				if (br != null) {
					br.close();
				}
			}
		}
		catch(Exception e) { // exception catched
			e.printStackTrace();
		}
		
		return lines;
	}
	
	public static void downloadWebFileToLocaFile(URL link, Path path) {
					
		try {
			URLConnection con = link.openConnection();
			Files.copy(con.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		}
		catch(Exception e) { // exception catched
			e.printStackTrace();
		}
	}
}
