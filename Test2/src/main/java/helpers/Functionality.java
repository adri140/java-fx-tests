package helpers;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Queue;

import clases.NumberOfCombination;
import clases.Ticket;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Functionality {
	
	// configuration
	private static final String CONFIGURATIONFILEPATH = "src/main/resources/configuration.txt";
	private static URL sourceWebFile;
	private static Path localSourceFile;
	public static String appName;
	
	//objects
	public static ObservableList<Ticket> allTickets = FXCollections.observableArrayList();
	public static NumberOfCombination[] allNumbers = new NumberOfCombination[49];
	
	public static void workWithTheTickets() {
		
		if (allTickets.size() > 0) {
			for (Ticket t : allTickets) {
				for (int i = 0; i < t.getArrayNumbers().length; i++) {
					if (allNumbers[t.getArrayNumbers()[i] - 1] == null) allNumbers[t.getArrayNumbers()[i] - 1] = new NumberOfCombination(t.getArrayNumbers()[i]);
					
					allNumbers[t.getArrayNumbers()[i] - 1].allCounted();
					
					for (int p = 0; p < t.getArrayNumbers().length; p++) {
						if (p != i) {
							if (allNumbers[t.getArrayNumbers()[i] - 1].getOthersNumbersArr()[t.getArrayNumbers()[p] - 1] == null) {
								allNumbers[t.getArrayNumbers()[i] - 1].getOthersNumbersArr()[t.getArrayNumbers()[p] - 1] = new NumberOfCombination(t.getArrayNumbers()[p]);
							}
							allNumbers[t.getArrayNumbers()[i] - 1].getOthersNumbersArr()[t.getArrayNumbers()[p] - 1].allCounted();
						}
					}
				}
			}
			
			for (NumberOfCombination c : allNumbers) { // for show in console
				c.calcPercentage(allTickets.size()); // calculamos el porcentage de vezes que ha salido este numero en los tickets
				for ( NumberOfCombination ot : c.getCalculatedOtherNumbers()) {
					ot.calcPercentage(c.getCounted()); // calculamos el porcentaje de veces que ha salido un numero con el numero anterior
				}
				//System.out.println(c);
			}
		}
	}
	
	/**
	 * Charge all tickets of source local file to the application.
	 * @return ObservableList<Ticket> with all tickets.
	 */
	public static ObservableList<Ticket> chargeAllTickets() {
		try {
			allTickets.clear();
			Queue<String> lines = Readers.readFile(localSourceFile.toFile());
						
			for (String line : lines) {
				if (line.charAt(0) >= 48 && line.charAt(0) <= 57) {
					allTickets.add(new Ticket(line, ","));
				}
			}
			
			workWithTheTickets();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return allTickets;
	}
	
	/**
	 * Update the local source file from web and charge the data to application.
	 */
	public static void updateSourceFile() {
		try {
			Readers.downloadWebFileToLocaFile(sourceWebFile, localSourceFile);
			chargeAllTickets();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This function read the configuration file and assign the values to variables.
	 * @return Boolean
	 */
	public static boolean chargeConfiguration() {
		
		boolean result = false;
		try {
			File configuration = new File(CONFIGURATIONFILEPATH);
			Queue<String> lines = Readers.readFile(configuration);
			
			do {
				String line = lines.poll();
				String[] params = line.split(" ");
				
				switch(params[0]) {
					case "sourceDownloadResults:":
						sourceWebFile = new URL(params[1]);
						break;
					case "locationOfFileInThisMachine:":
						localSourceFile = Paths.get(params[1]);
						break;
					case "appName:":
						appName = params[1].replaceAll("_", " ");
						break;
				}
			} while (lines.size() > 0);
			result = true;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static void showConfigurationFile() {
		try {
			Desktop.getDesktop().open(new File(CONFIGURATIONFILEPATH));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
