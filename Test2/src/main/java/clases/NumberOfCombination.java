package clases;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class NumberOfCombination {
	private int number;
	private long counted;	
	private NumberOfCombination[] otherNumbersArr = new NumberOfCombination[49];
	private ObservableList<NumberOfCombination> calculatedOtherNumbers = FXCollections.observableArrayList();
	private int percentage;
	
	public NumberOfCombination(int number) {
		this.number = number;
	}
	
	public void setCounted(long counted) {
		this.counted = counted;
	}
	
	public long allCounted() {
		counted ++;
		return counted;
	}
	
	public long getCounted() {
		return counted;
	}
	
	public int getNumber() {
		return number;
	}
	
	public void setOtherNumbersArr(NumberOfCombination[] otherNumbersArr) {
		this.otherNumbersArr = otherNumbersArr;
	}
	
	public NumberOfCombination[] getOthersNumbersArr() {
		return otherNumbersArr;
	}
	
	public void calcPercentage(long max) {
		this.percentage = (int) (this.counted * 100 / max);
	}
	
	public String getPercentage() {
		return percentage + "%";
	}
	
	public void setCalculatedOtherNumbers(ObservableList<NumberOfCombination> calculatedOtherNumbers) {
		this.calculatedOtherNumbers.clear();
		this.calculatedOtherNumbers.addAll(calculatedOtherNumbers);
	}
	
	public ObservableList<NumberOfCombination> getCalculatedOtherNumbers() {
		
		if (calculatedOtherNumbers.size() == 0) {
			long maxCounted = 0;		
			List<NumberOfCombination> resultTMP = new ArrayList<NumberOfCombination>();
			
			for (int i = 0; i < 3; i++) {
				for (NumberOfCombination n : otherNumbersArr) {
					if (!calculatedOtherNumbers.contains(n) && n != null) {
						if (n.getCounted() > maxCounted) {
							resultTMP.clear();
							maxCounted = n.getCounted();
							
							resultTMP.add(n);
						} else {
							if (n.getCounted() == maxCounted) {
								resultTMP.add(n);
							}
						}
					}
				}
				calculatedOtherNumbers.addAll(resultTMP);
				maxCounted = 0;
			}
		}
				
		return calculatedOtherNumbers;
	}
	
	@Override
	public String toString() {
		String str = "Number: " + getNumber() + "\n";
		
		str += "maxCounted: " + getCounted() + "\n";
		
		for(NumberOfCombination n : otherNumbersArr) {
			if (n != null) str += "OtherNumber: " + n.getNumber() + ", maxCounted: " + n.getCounted() + "\n";
		}
		
		str += "OthersNumbersFinished: ";
		
		for (NumberOfCombination n : getCalculatedOtherNumbers()) {
			str += n.getNumber() + " - " + n.getCounted() + ", ";
		}
		
		str += "\n\n";
		
		return str;
	}
}
