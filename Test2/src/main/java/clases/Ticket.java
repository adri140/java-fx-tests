package clases;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Ticket {
	
	private int[] numbers = new int[6];
	
	private int repayment;
	
	private MyDate date;
	
	public Ticket(String line, String separator) {
		String[] separedLine = line.split(separator);
		
		try {
			//Date date = new SimpleDateFormat("dd/MM/yyyy").parse(separedLine[0]);
			this.date = new MyDate(new SimpleDateFormat("dd/MM/yyyy").parse(separedLine[0]));
			this.repayment = Integer.parseInt(separedLine[8]);
			
			for (int pos = 0; pos < this.numbers.length; pos++) {
				this.numbers[pos] = Integer.parseInt(separedLine[pos + 1]);
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Ticket(MyDate date, int[] numbers, int repayment) {
		this.date = date;
		this.numbers = this.numbers.length == numbers.length ? numbers : this.numbers;
		this.repayment = repayment >= 0 && repayment <= 9? repayment : 0;
	}

	public String getNumbers() {
		String combination = "";
		
		for (int number : this.numbers) {
			combination += number <= 9? "0" + number + ", " : number + ", ";
		}
		return combination;
	}
	
	public int[] getArrayNumbers() {
		return this.numbers;
	}

	public int getRepayment() {
		return repayment;
	}

	public MyDate getDate() {
		return this.date;
	}
	
	/*public String getDate() {
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy"); 
		return formater.format(this.date);
	}*/
	
	@Override
	public String toString() {
		String returnString = "Date: " + this.date.toString();
		
		returnString += " Numbers: ";
		for(int value : this.numbers) {
			returnString += value + " ";
		}
		
		return returnString + "Repayment: " + this.repayment;
	}
}
