package clases;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyDate extends Date {
	
	public MyDate(Date t) {
		super(t.getTime());
	}

	@Override
	public String toString() {
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy"); 
		return formater.format(this);
	}

}
