# Java FX Tests
**Instalacion de Java FX en eclipse**
1. Descargamos e instalamos Java FX en "https://www.eclipse.org/efxclipse/install.html"
2. Descargamos el scene builder en "https://gluonhq.com/products/scene-builder/"
	- Descargar la versión para java 11, para plataformas Windows.
3. En eclipse vamos a "window" --> "preferences" --> "javafx" y donde pone "scenebuilder 
   execancutable" ponemos la ruta del ejecutable, por defecto 
   "C:\Program Files\SceneBuilder\SceneBuilder.exe"
